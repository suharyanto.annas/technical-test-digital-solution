package id.co.solusidigital.ui.base.listeners

import id.co.solusidigital.data.dto.recipes.RecipesItem

interface RecyclerItemListener {
    fun onItemSelected(recipe : RecipesItem)
}
