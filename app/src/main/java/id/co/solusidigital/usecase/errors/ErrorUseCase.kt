package id.co.solusidigital.usecase.errors

import id.co.solusidigital.data.error.Error

interface ErrorUseCase {
    fun getError(errorCode: Int): Error
}
