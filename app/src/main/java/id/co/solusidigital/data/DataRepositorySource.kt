package id.co.solusidigital.data

import id.co.solusidigital.data.dto.recipes.Recipes
import id.co.solusidigital.data.dto.login.LoginRequest
import id.co.solusidigital.data.dto.login.LoginResponse
import kotlinx.coroutines.flow.Flow

interface DataRepositorySource {
    suspend fun requestRecipes(): Flow<Resource<Recipes>>
    suspend fun addToFavourite(id: String): Flow<Resource<Boolean>>
    suspend fun removeFromFavourite(id: String): Flow<Resource<Boolean>>
    suspend fun isFavourite(id: String): Flow<Resource<Boolean>>
}
