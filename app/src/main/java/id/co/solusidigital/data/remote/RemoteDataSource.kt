package id.co.solusidigital.data.remote

import id.co.solusidigital.data.Resource
import id.co.solusidigital.data.dto.recipes.Recipes

internal interface RemoteDataSource {
    suspend fun requestRecipes(): Resource<Recipes>
}
