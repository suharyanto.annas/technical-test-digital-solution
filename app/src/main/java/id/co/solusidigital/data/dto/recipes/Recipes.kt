package id.co.solusidigital.data.dto.recipes

data class Recipes(val recipesList: ArrayList<RecipesItem>)
