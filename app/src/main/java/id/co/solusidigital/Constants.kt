package id.co.solusidigital

const val BASE_URL = "https://newsapi.org/v2/everything?q=tesla&from=2022-01-22&sortBy=publishedAt&apiKey=6986e3380de3441c9b79a2360d581bfa"
const val RECIPE_ITEM_KEY = "RECIPE_ITEM_KEY"
const val SHARED_PREFERENCES_FILE_NAME = "solusidigital"
const val FAVOURITES_KEY = "favourites"
